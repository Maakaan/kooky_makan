package com.example.makanalioucoulibaly.kooky_searchpage;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by makanalioucoulibaly on 08/03/2017.
 */

public class ImageAdapter extends BaseAdapter {

    private Context mContext;
    public int mPosition ;
    private LayoutInflater inflater;
    public View mVu;


    public int [] images= {R.drawable.lait, R.drawable.oeuf, R.drawable.farine, R.drawable.tomate, R.drawable.steak,
            R.drawable.pomme_de_terre, R.drawable.persil, R.drawable.poulet_poitrine, R.drawable.poulet_pilon, R.drawable.poulet_aile, R.drawable.pate, R.drawable.riz, R.drawable.pain, R.drawable.oignon, R.drawable.poivron_vert, R.drawable.poivron_rouge, R.drawable.poivron_jaune};

    public ImageAdapter(Context c, int pos)
    {
        mContext = c;
        mPosition = pos;
        //mVu = nVu;

    }
    @Override
    public int getCount() {
        
        return images.length;
    }

    @Override
    public Object getItem(int a) {
        return images[0];
    }

    @Override
    public long getItemId(int b) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView img;

        if (convertView == null){
            img = new ImageView(mContext);
            img.setLayoutParams(new GridView.LayoutParams(160,160));
            img.setScaleType(ImageView.ScaleType.CENTER_CROP);

        }else
        {
            img= (ImageView) convertView;
        }

        img.setImageResource(images[position]);
        return img;













       /* position = mPosition;


        View imageView = convertView;
        mVu =imageView;

        if (convertView == null) {


            inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            imageView =inflater.inflate(R.layout.item_single, null);
        }

        ImageView image = (ImageView) imageView.findViewById(R.id.icon);
        image.setImageResource(images[position]);

        return imageView;
        */

    }
    }

